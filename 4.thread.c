#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include "tempo.h"
#include <pthread.h>
#include <math.h>


#define QT 4
char linha[40];
int intervalo;
int ok, cont;

void *crash (void *arg);

void main(int argc, char *argv[]){

	intervalo = (int)95/QT; //calcula o intervalo de caracteres entre as threads

	FILE *fptr; //cria um ponteiro pra arquivo
	int i, pos, a, tam;

	if ((fptr = fopen ("password.txt","rb")) == NULL){ //tenta abrir o arquivo
		printf ("Nao posso abrir o arquivo");
		exit(1);
	}

	fgets(linha,40,fptr); //armazena no vet linha 40 caracteres do arquivo
	fclose(fptr); //fecha o arquivo

	tam = strlen(linha); //pega o tamanho real da linha

	tempo1();
	pthread_t t[QT]; //cria vetor de threads
	// int start[QT];

	for(i = 0; i < QT; i++)
	{	
		int *threadNum = malloc(sizeof(*threadNum));
		if ( threadNum == NULL ) {
            fprintf(stderr, "Couldn't allocate memory for thread arg.\n");
            exit(EXIT_FAILURE);
        }

        *threadNum = i;

		pthread_create(&t[i], NULL, *crash, threadNum);
	}
	
	for(i = 0; i < QT; i++)
	{
		pthread_join(t[i], NULL);
	}	
	
	tempo2();
	tempoFinal("segundos", argv[0], MSGLOG);
}


void *crash (void *arg){
	char senha[40], converter;
	int i = *((int*)arg); //pega o argumento da thread que é o seu índice

	int inicio;
	senha[strlen(linha)]='\0';

	/*
	* Evita o erro de divisão por zero
	*/

	if(i==0){
		inicio = 32;
	}else{
		inicio = intervalo*i + 32;
	}

	/*
	* Detecta a última thread e força ela ir até o fim da tabela
	*/
	int fim;
	if (i == (QT-1)){
		fim = 127;
	}else{
		fim = inicio + (intervalo-1);
	}

	printf("INICIO: %d FIM: %d\n",inicio, fim);


	// while (1) {

		for (int c = inicio; c < fim; c++) {   				//for 1o. char
			senha[0] = c; 

	   		 for (int d = 32; d < 127; d++){    			//for 2o. char
				 senha[1] = d;

		   		 for (int e = 32; e < 127; e++) {    		//for 3o. char
					 senha[2] = e;
				
					for (int f = 32; f < 127; f++) {  	//for 4o. char
						senha[3] = f;

						cont ++;

						// printf("\nTESTE %d : %s (%ld) : SENHA: %s (%ld) -- NA THREAD: %d",
						// 		cont, 
						// 		senha, 
						// 		strlen(senha), 
						// 		linha,
						// 		strlen(linha),
						// 		i);

						ok = strcmp(senha, linha);

						if (ok == 0){
							printf("\n\n\n\nsenha encontrada: %s",senha);
							printf("\n\n\n\nnúmero de testes: %d\n\n\n",cont);

							tempo2();
							tempoFinal("segundos", 0, MSGLOG);

							exit(0);
						}//final do if

					}//final do for  4o. char

				 }//final do for  3o. char

			 }//final do for  2o. char

		}//final do for  1o. char
pthread_exit(NULL);
	//}//final do while
}
